package model;

import java.awt.Point;
import java.util.ArrayList;

public class Bispo extends Piece {

	public Bispo() {
		
	}

	public Bispo(String imagePath) {
		super(imagePath);
	}

	@Override
	public ArrayList<Point> getPosition(int row, int col) {
		zerarPosition();
		for(int i = 0; i < 8; i++){
				addNewPosition(row + i, col + i);
				addNewPosition(row + i, col - i);
				addNewPosition(row - i, col + i);
				addNewPosition(row - i, col - i);
		}
		return super.getPosition(row, col);
	}
}
