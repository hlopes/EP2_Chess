package model;

import java.awt.Point;
import java.util.ArrayList;


public class Piece {

	public static final String EMPTY_PATH = "";
	public static final int COL_NUMBER = 8;
	
	private String imagePath;
	private String color;
	private ArrayList<Point> position;
	
	public String getColor() {
		return color;
	}

	public void setColor(){
		if(this.imagePath.contains("White")){
			this.color = "White";
		}else{
			this.color = "Yellow";
		}
	}
//	public void setColor(String color) {
//		this.color = color;
//	}

	public Piece() {
		super();
	}
	
	public Piece(String imagePath) {
		super();
		this.imagePath = imagePath;
		position = new ArrayList<>();
		setColor();
	}
	

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}
	
	public void removerImagePath(String imagePath){
		this.imagePath = EMPTY_PATH;
	}
	
	public boolean haveImagePath() {
		return this.imagePath != EMPTY_PATH;
	}
	
	public void setPosition(ArrayList<Point> newPosition){
		this.position = newPosition;
	}
	
	public ArrayList<Point> getPosition(int row, int col){
		return position;
	}
	
	public void zerarPosition(){
		position.clear();
	}
	
	public void addNewPosition(int row, int col){
		if(row<8 && row>=0 && col<8 && col>=0){
			Point newPosition = new Point(row, col);
			position.add(newPosition);
		}
	}
	

}
