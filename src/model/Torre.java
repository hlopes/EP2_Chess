package model;

import java.awt.Point;
import java.util.ArrayList;

public class Torre extends Piece {

	public Torre() {
		
	}

	public Torre(String imagePath) {
		super(imagePath);
		
	}
	
	@Override
	public ArrayList<Point> getPosition(int row, int col) {
		zerarPosition();
			for(int i = row+1; i<8; i++){
				addNewPosition(i, col);
			}
			
			for(int i = row-1; i>=0; i--){
				addNewPosition(i, col);
			}
			
			for(int i = col+1; i<8; i++){
				addNewPosition(row, i);
			}
			
			for(int i = col-1; i>=0; i--){
				addNewPosition(row, i);
			}
			
		return super.getPosition(row, col);
	}
	

}
