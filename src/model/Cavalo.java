package model;

import java.awt.Point;
import java.util.ArrayList;

public class Cavalo extends Piece {

	public Cavalo() {
	
	}

	public Cavalo(String imagePath) {
		super(imagePath);
		
	}
	
	@Override
	public ArrayList<Point> getPosition(int row, int col) {
		zerarPosition();
		
		for(int i = -1; i<=1; i++){
			for(int j = -1; j<=1; j++){
				if(i!=0 && j!=0){
						addNewPosition(row - 2*i, col - j);
						addNewPosition(row - i, col - 2*j);
					
				}
			}
		}
		return super.getPosition(row, col);
	}

}
