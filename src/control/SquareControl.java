package control;
import java.awt.Point;
import java.awt.Color;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import model.Square;
import model.Square.SquareEventListener;

public class SquareControl implements SquareEventListener {

	public static final int ROW_NUMBER = 8;
	public static final int COL_NUMBER = 8;
	public static final int MAX_INDEX = 64;
	public static final int MIN_INDEX = 0;

	public static final Color DEFAULT_COLOR_ONE = Color.WHITE;
	public static final Color DEFAULT_COLOR_TWO = Color.darkGray;
	public static final Color DEFAULT_COLOR_HOVER = Color.BLUE;
	public static final Color DEFAULT_COLOR_SELECTED = Color.GREEN;
	public static final Color DEFAULT_COLOR_POSS = new Color(0, 200, 150);
	

	public static final Square EMPTY_SQUARE = null;

	private Color colorOne;
	private Color colorTwo;
	private Color colorHover;
	private Color colorSelected;
	private Color colorPossibilitie;
	
	private Square selectedSquare;
	private ArrayList<Square> squareList;
	private ArrayList<Square> squarePossibilities;

	public SquareControl() {
		this(DEFAULT_COLOR_ONE, DEFAULT_COLOR_TWO, DEFAULT_COLOR_HOVER,
				DEFAULT_COLOR_SELECTED, DEFAULT_COLOR_POSS);
	}

	public SquareControl(Color colorOne, Color colorTwo, Color colorHover,
			Color colorSelected, Color colorPossibilitie) {
		this.colorOne = colorOne;
		this.colorTwo = colorTwo;
		this.colorHover = colorHover;
		this.colorSelected = colorSelected;
		this.colorPossibilitie = colorPossibilitie;

		this.squareList = new ArrayList<>();
		this.squarePossibilities = new ArrayList<>();
		createSquares();
	}

	public void resetColor(Square square) {
		int index = this.squareList.indexOf(square);
		int row = index / COL_NUMBER;
		int col = index % COL_NUMBER;
		
		try {
			square.setColor(getGridColor(row, col));
		} catch (Exception e) {
		}
	}

	@Override
	public void onHoverEvent(Square square) {
		if(square.getColor() != this.colorSelected && square.getColor() != this.colorPossibilitie){
			square.setColor(this.colorHover);
		}
	}

	@Override
	public void onSelectEvent(Square square) {
		if (haveSelectedCellPanel()) {
			if (!this.selectedSquare.equals(square) && square.getColor().equals(this.colorPossibilitie)) {
						try {
							moveContentOfSelectedSquare(square);
						} catch (Exception e) {
						}
			} else {
				unselectSquare(square);
			}
		} else {
			selectSquare(square);
		}
	}

	@Override
	public void onOutEvent(Square square) {
		if (square.getColor() != this.colorSelected && square.getColor()!= this.colorPossibilitie) {
			resetColor(square);
		} else {
			if(this.selectedSquare.equals(square)){
				square.setColor(this.colorSelected);
			}
		}
	}

	public Square getSquare(int row, int col) {
		return this.squareList.get((row * COL_NUMBER) + col);
	}

	public ArrayList<Square> getSquareList() {
		return this.squareList;
	}

	public Color getGridColor(int row, int col) {
		if ((row + col) % 2 == 0) {
			return this.colorOne;
		} else {
			return this.colorTwo;
		}
	}

	private void addSquare() {
		Square square = new Square();
		this.squareList.add(square);
		resetColor(square);
		resetPosition(square);
		square.setSquareEventListener(this);
	}

	private void resetPosition(Square square) {
		int index = this.squareList.indexOf(square);
		int row = index / COL_NUMBER;
		int col = index % COL_NUMBER;

		square.getPosition().setLocation(row, col);
	}

	private boolean haveSelectedCellPanel() {
		return this.selectedSquare != EMPTY_SQUARE;
	}

	private void moveContentOfSelectedSquare(Square square) {
		if(square.havePiece() && square.getPiece().getClass().getSimpleName().equalsIgnoreCase("Rei")){
			JOptionPane.showMessageDialog(null, "Xeque-mate!");
		}
		square.setPiece(this.selectedSquare.getPiece());
		this.selectedSquare.removePiece();
		unselectSquare(square);
	}

	private void selectSquare(Square square) {
		if (square.havePiece()) {
			this.selectedSquare = square;
			this.selectedSquare.setColor(this.colorSelected);
			try {
				setPossibilities(square);
			} catch (Exception e) {
				
			}
		}
	}
	
	private void setPossibilities(Square square){
		String className = square.getPiece().getClass().getSimpleName();
		if(className.equalsIgnoreCase("Peao")){
			getPeaoPossibilities(square);
		}
		else if(className.equalsIgnoreCase("Torre")){
			getTorrePossibilities(square);
		}
		else if(className.equalsIgnoreCase("Bispo")){
			getBispoPossibilities(square);
		}
		else if(className.equalsIgnoreCase("Cavalo")){
			getCavaloPossibilities(square);
		}
		else if(className.equalsIgnoreCase("Rainha")){
			getRainhaPossibilities(square);
		}
		else if(className.equalsIgnoreCase("Rei")){
			getReiPossibilities(square);
		}
		setColorPossibilities();
	}
	
	private void getReiPossibilities(Square square){
		int row = square.getPosition().x;
		int col = square.getPosition().y;
		
		ArrayList<Point> possibilities = square.getPiece().getPosition(row, col);
		
		for(int i = 0; i < possibilities.size(); i++){
			int rows = possibilities.get(i).x;
			int cols = possibilities.get(i).y;

			Square squarep = getSquare(rows, cols);
			if(square.getPiece().getColor().equalsIgnoreCase("White")){
				if(squarep.havePiece() && squarep.getPiece().getColor().equalsIgnoreCase("Yellow")){
					this.squarePossibilities.add(squarep);
				}
				else if(!squarep.havePiece()){
					this.squarePossibilities.add(squarep);
				}
			}
			
			else if(square.getPiece().getColor().equalsIgnoreCase("Yellow")){
				if(squarep.havePiece() && squarep.getPiece().getColor().equalsIgnoreCase("White")){
					this.squarePossibilities.add(squarep);
				}else if(!squarep.havePiece()){
					this.squarePossibilities.add(squarep);
				}
			}
			
		}
	}
	
	private void getRainhaPossibilities(Square square){
		getBispoPossibilities(square);
		getTorrePossibilities(square);
		
	}
	
	private void getCavaloPossibilities(Square square){
		int row = square.getPosition().x;
		int col = square.getPosition().y;
		
		ArrayList<Point> possibilities = square.getPiece().getPosition(row, col);
		
		for(int i = 0; i < possibilities.size(); i++){
			int rows = possibilities.get(i).x;
			int cols = possibilities.get(i).y;

			Square squarep = getSquare(rows, cols);
			if(square.getPiece().getColor().equalsIgnoreCase("White")){
				if(squarep.havePiece() && squarep.getPiece().getColor().equalsIgnoreCase("Yellow")){
					this.squarePossibilities.add(squarep);
				}
				else if(!squarep.havePiece()){
					this.squarePossibilities.add(squarep);
				}
			}
			
			else if(square.getPiece().getColor().equalsIgnoreCase("Yellow")){
				if(squarep.havePiece() && squarep.getPiece().getColor().equalsIgnoreCase("White")){
					this.squarePossibilities.add(squarep);
				}else if(!squarep.havePiece()){
					this.squarePossibilities.add(squarep);
				}
			}
			
		}
	}
	private void getBispoPossibilities(Square square){
		int row = square.getPosition().x;
		int col = square.getPosition().y;
		boolean checkFL = false;
		boolean checkFR = false;
		boolean checkBL = false;
		boolean checkBR = false;
		
		ArrayList<Point> possibilities = square.getPiece().getPosition(row, col);
		
		for(int i = 0; i < possibilities.size(); i++){
			int rows = possibilities.get(i).x;
			int cols = possibilities.get(i).y;

			Square squarep = getSquare(rows, cols);
			
			if(squarep.havePiece()){
				if(square.getPiece().getColor().equalsIgnoreCase("White")){
					if(squarep.getPiece().getColor().equalsIgnoreCase("Yellow")){
						if((rows - row) < 0 && (cols-col)<0 && checkFL == false){
							checkFL = true;
							this.squarePossibilities.add(squarep);
						}
						else if((rows - row) < 0 && (cols-col)>0 && checkFR == false){
							checkFR = true;
							this.squarePossibilities.add(squarep);
						}
						if((rows - row) > 0 && (cols-col)<0 && checkBL == false){
							checkBL = true;
							this.squarePossibilities.add(squarep);
						}
						else if((rows - row) > 0 && (cols-col)>0 && checkBR == false){
							checkBR = true;
							this.squarePossibilities.add(squarep);
						}
					}else{
						if((rows - row) < 0 && (cols-col)<0 && checkFL == false){
							checkFL = true;
						}
						else if((rows - row) < 0 && (cols-col)>0 && checkFR == false){
							checkFR = true;
						}
						if((rows - row) > 0 && (cols-col)<0 && checkBL == false){
							checkBL = true;
						}
						else if((rows - row) > 0 && (cols-col)>0 && checkBR == false){
							checkBR = true;
						}
					}
				}else if(square.getPiece().getColor().equalsIgnoreCase("Yellow")){
					if(squarep.getPiece().getColor().equalsIgnoreCase("White")){
						if((rows - row) < 0 && (cols-col)<0 && checkFL == false){
							checkFL = true;
							this.squarePossibilities.add(squarep);
						}
						else if((rows - row) < 0 && (cols-col)>0 && checkFR == false){
							checkFR = true;
							this.squarePossibilities.add(squarep);
						}
						if((rows - row) > 0 && (cols-col)<0 && checkBL == false){
							checkBL = true;
							this.squarePossibilities.add(squarep);
						}
						else if((rows - row) > 0 && (cols-col)>0 && checkBR == false){
							checkBR = true;
							this.squarePossibilities.add(squarep);
						}
					}else{
						if((rows - row) < 0 && (cols-col)<0 && checkFL == false){
							checkFL = true;
						}
						else if((rows - row) < 0 && (cols-col)>0 && checkFR == false){
							checkFR = true;
						}
						if((rows - row) > 0 && (cols-col)<0 && checkBL == false){
							checkBL = true;
						}
						else if((rows - row) > 0 && (cols-col)>0 && checkBR == false){
							checkBR = true;
						}
					}
				}
			}else{
				if((rows - row) < 0 && (cols-col)<0 && checkFL == false){
					this.squarePossibilities.add(squarep);
				}
				else if((rows - row) < 0 && (cols-col)>0 && checkFR == false){
					this.squarePossibilities.add(squarep);
				}
				if((rows - row) > 0 && (cols-col)<0 && checkBL == false){
					this.squarePossibilities.add(squarep);
				}
				else if((rows - row) > 0 && (cols-col)>0 && checkBR == false){
					this.squarePossibilities.add(squarep);
				}
			}
		}
	}
	
	private void getTorrePossibilities(Square square){
		int row = square.getPosition().x;
		int col = square.getPosition().y;
		boolean checkF = false;
		boolean checkB = false;
		boolean checkR = false;
		boolean checkL = false;
		
		ArrayList<Point> possibilities = square.getPiece().getPosition(row, col);
		
		for(int i = 0; i<possibilities.size(); i++){
			int rows = possibilities.get(i).x;
			int cols = possibilities.get(i).y;

			Square squarep = getSquare(rows, cols);
			if(squarep.havePiece()){
				if(square.getPiece().getColor().equalsIgnoreCase("White")){
					if(squarep.getPiece().getColor().equalsIgnoreCase("Yellow")){
						if((rows - row) < 0 && checkF == false){
							checkF = true;
							this.squarePossibilities.add(squarep);
						}
						else if((rows - row) > 0 && checkB == false){
							checkB = true;
							this.squarePossibilities.add(squarep);
						}
						if((cols - col) < 0 && checkL == false){
							checkL = true;
							this.squarePossibilities.add(squarep);
						}
						else if((cols - col) > 0 && checkR == false){
							checkR = true;
							this.squarePossibilities.add(squarep);
						}
					}else{
						if((rows - row) < 0 && checkF == false){
							checkF = true;
						}
						else if((rows - row) > 0 && checkB == false){
							checkB = true;
						}
						if((cols - col) < 0 && checkL == false){
							checkL = true;
						}
						else if((cols - col) > 0 && checkR == false){
							checkR = true;
						}
					}

				}else if(square.getPiece().getColor().equalsIgnoreCase("Yellow")){
					if(squarep.getPiece().getColor().equalsIgnoreCase("White")){
						if((rows - row) < 0 && checkF == false){
							checkF = true;
							this.squarePossibilities.add(squarep);
						}
						else if((rows - row) > 0 && checkB == false){
							checkB = true;
							this.squarePossibilities.add(squarep);
						}
						if((cols - col) < 0 && checkL == false){
							checkL = true;
							this.squarePossibilities.add(squarep);
						}
						else if((cols - col) > 0 && checkR == false){
							checkR = true;
							this.squarePossibilities.add(squarep);
						}
					}else{
						if((rows - row) < 0 && checkF == false){
							checkF = true;
						}
						else if((rows - row) > 0 && checkB == false){
							checkB = true;
						}
						if((cols - col) < 0 && checkL == false){
							checkL = true;
						}
						else if((cols - col) > 0 && checkR == false){
							checkR = true;
						}
					}
				}
			}else{
				if((rows - row) < 0 && checkF == false){
					this.squarePossibilities.add(squarep);
				}
				if((rows - row) > 0 && checkB == false){
					this.squarePossibilities.add(squarep);
				}
				if((cols - col) < 0 && checkL == false){
					this.squarePossibilities.add(squarep);
				}
				if((cols - col) > 0 && checkR == false){
					this.squarePossibilities.add(squarep);
				}
			}

		}
	}
	
	private void getPeaoPossibilities(Square square){
		int row = square.getPosition().x;
		int col = square.getPosition().y;
		ArrayList<Point> possibilities = square.getPiece().getPosition(row, col);
		
		for(int i = 0; i<possibilities.size(); i++){
			int rows = possibilities.get(i).x;
			int cols = possibilities.get(i).y;
			Square squarep = getSquare(rows, cols);
	
			if(square.getPiece().getColor().equalsIgnoreCase("White")){
				if(row - rows == 2 && row == 6){
					if(!squarep.havePiece() && !this.squareList.get((row-1)*COL_NUMBER + col).havePiece()){
						this.squarePossibilities.add(squarep);
					}
				}
				if(row - rows == 1){
					if(!squarep.havePiece() && col - cols == 0){
						this.squarePossibilities.add(squarep);
					}
					if(squarep.havePiece() && col - cols != 0 && squarep.getPiece().getColor().equalsIgnoreCase("Yellow")){
						this.squarePossibilities.add(squarep);
					}
				}
			}else{
				if(rows - row == 2 && row == 1){
					if(!squarep.havePiece() && !this.squareList.get((row+1)*COL_NUMBER + col).havePiece()){
						this.squarePossibilities.add(squarep);
					}
				}
				if(rows - row == 1){
					if(!squarep.havePiece() && cols - col == 0){
						this.squarePossibilities.add(squarep);
					}
					if(squarep.havePiece() && cols - col != 0 && squarep.getPiece().getColor().equalsIgnoreCase("White")){
						this.squarePossibilities.add(squarep);
					}
				}
			}
			
		}
	}
	
	
	private void setColorPossibilities(){
		for(int i=0; i<(this.squarePossibilities.size()); i++){
			this.squarePossibilities.get(i).setColor(colorPossibilitie);
		}
	}
	
	private void resetPossibilities(){
		for(int i=0; i<(this.squarePossibilities.size()); i++){
			resetColor(this.squarePossibilities.get(i));
		}
		this.squarePossibilities.clear();
	}

	private void unselectSquare(Square square) {
		resetColor(this.selectedSquare);
		resetPossibilities();
		this.selectedSquare = EMPTY_SQUARE;
	}
	

	private void createSquares() {
		for (int i = 0; i < (ROW_NUMBER * COL_NUMBER); i++) {
			addSquare();
		}
	}
}
