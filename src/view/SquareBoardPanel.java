package view;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.ArrayList;

import javax.swing.JPanel;

import model.Bispo;
import model.Cavalo;
import model.Peao;
import model.Rainha;
import model.Rei;
import model.Square;
import model.Piece;
import model.Torre;
import control.SquareControl;

public class SquareBoardPanel extends JPanel {

	private static final long serialVersionUID = 7332850110063699836L;

	private SquareControl squareControl;
	private ArrayList<SquarePanel> squarePanelList;

	public SquareBoardPanel() {
		setLayout(new GridBagLayout());
		this.squarePanelList = new ArrayList<SquarePanel>();

		initializeSquareControl();
		initializeGrid();
		initializePiecesInChess();
	}

	private void initializeSquareControl() {
		Color colorOne = Color.WHITE;
		Color colorTwo = Color.darkGray;
		Color colorHover = Color.BLUE;
		Color colorSelected = Color.GREEN;
		Color colorPossibilitie = new Color(0, 200, 150);

		this.squareControl = new SquareControl(colorOne, colorTwo, colorHover,
				colorSelected, colorPossibilitie);
	}

	private void initializeGrid() {
		GridBagConstraints gridBag = new GridBagConstraints();

		Square square;
		for (int i = 0; i < this.squareControl.getSquareList().size(); i++) {
			square = this.squareControl.getSquareList().get(i);
			gridBag.gridx = square.getPosition().y;
			gridBag.gridy = square.getPosition().x;

			SquarePanel squarePanel = new SquarePanel(square);

			add(squarePanel, gridBag);
			this.squarePanelList.add(squarePanel);
		}

	}

	private void initializePiecesInChess() {
		
		Piece peao1 = new Peao("icon/Yellow P_48x48.png");
		
		for (int i = 0; i < SquareControl.COL_NUMBER; i++) {
			this.squareControl.getSquare(1, i).setPiece(peao1);
		}
		
		String peacePath = "icon/Yellow R_48x48.png";
		Piece Torre1 = new Torre(peacePath);
		this.squareControl.getSquare(0, 0).setPiece(Torre1);
		this.squareControl.getSquare(0, 7).setPiece(Torre1);

		peacePath = "icon/Yellow N_48x48.png";
		Piece Cavalo1 = new Cavalo(peacePath);
		this.squareControl.getSquare(0, 1).setPiece(Cavalo1);
		this.squareControl.getSquare(0, 6).setPiece(Cavalo1);

		peacePath = "icon/Yellow B_48x48.png";
		Piece Bispo1 = new Bispo(peacePath);
		this.squareControl.getSquare(0, 2).setPiece(Bispo1);
		this.squareControl.getSquare(0, 5).setPiece(Bispo1);

		peacePath = "icon/Yellow Q_48x48.png";
		Piece Rainha1 = new Rainha(peacePath);
		this.squareControl.getSquare(0, 4).setPiece(Rainha1);

		peacePath = "icon/Yellow K_48x48.png";
		Piece Rei1 = new Rei(peacePath);
		this.squareControl.getSquare(0, 3).setPiece(Rei1);

		
		peacePath = "icon/White P_48x48.png";
		Piece Peao2 = new Peao(peacePath);
		for (int i = 0; i < SquareControl.COL_NUMBER; i++) {
			this.squareControl.getSquare(6, i).setPiece(Peao2);
		}
		
		peacePath = "icon/White R_48x48.png";
		Piece Torre2 = new Torre(peacePath);
		this.squareControl.getSquare(7, 0).setPiece(Torre2);
		this.squareControl.getSquare(7, 7).setPiece(Torre2);

		peacePath = "icon/White N_48x48.png";
		Piece Cavalo2 = new Cavalo(peacePath);
		this.squareControl.getSquare(7, 1).setPiece(Cavalo2);
		this.squareControl.getSquare(7, 6).setPiece(Cavalo2);

		peacePath = "icon/White B_48x48.png";
		Piece Bispo2 = new Bispo(peacePath);
		this.squareControl.getSquare(7, 2).setPiece(Bispo2);
		this.squareControl.getSquare(7, 5).setPiece(Bispo2);

		peacePath = "icon/White Q_48x48.png";
		Piece Rainha2 = new Rainha(peacePath);
		this.squareControl.getSquare(7, 4).setPiece(Rainha2);

		peacePath = "icon/White K_48x48.png";
		Piece Rei2 = new Rei(peacePath);
		this.squareControl.getSquare(7, 3).setPiece(Rei2);
	}
}
